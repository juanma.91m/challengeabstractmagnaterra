# ChallengeAbstractMagnaterra

## Problema a resolver

En una galaxia lejana, existen tres civilizaciones. Vulcanos, Ferengis y Betasoides. Cada civilización vive en paz en su respectivo planeta.

Dominan la predicción del clima mediante un complejo sistema informático.

Premisas: 

1-El planeta Ferengi se desplaza con una velocidad angular de 1 grado por día en sentido horario. Su distancia con respecto al sol es de 500km.
2-El planeta Betasoide se desplaza con una velocidad angular de 3 grados por día en sentido horario. Su distancia con respecto al sol es de 2000km.
3-El planeta Vulcano se desplaza con una velocidad angular de 5 grados por día en sentido anti-horario. Su distancia con respecto al sol es de 1000km.

Todas las orbitas son circulares.

Cuando los tres planetas están alineados entre sí y a su vez alineados con respecto al sol, el sistema solar experimenta un periodo de sequía. Cuando los tres planetas no están alineados, forman entre si un triángulo. Es sabido que en el momento en el que el sol se encuentra dentro del triángulo, el sistema solar experimenta un periodo de lluvia, teniendo este, un pico de intensidad cuando el perímetro del triángulo está en su máximo. Las condiciones óptimas de presión y temperatura se dan cuando los tres planetas están alineados entre sí, pero no están alineados con el sol.

Realizar:

Un programa informático para poder predecir en los próximos 10 años: 
1-¿Cuántos periodos de sequía habrá? 
2-¿Cuántos periodos de lluvia habrá y que día será el pico máximo de lluvia? 
3-¿Cuántos periodos de condiciones óptimas de presión y temperatura habrá?

Generar un modelo de datos con las condiciones de todos los días hasta 10 años en adelante utilizando un job para calcularlas. Generar una API REST la cual devuelve en formato JSON la condición climática del día consultado.

Generar y adjuntarla documentación considerada necesaria e importante.

Requerido: utilizar Java/Spring

Extra (deseable – no requerido):
1-Hostear el modelo de datos y la API REST en un cloud computing libre y enviar la URL para consultar el clima de un día en particular 



## Descripción

API REST sencilla que permite determinar el clima y pronostico en días del sistema Solar para periodos de tiempo determinado:

Cabe destacar que se supuso una posición inicial para el día 0 donde todos los planetas estan alineados en el eje vertical (ordenadas cartecianas), y desde ahí parten los desplazamientos.

A futuro se puede incorporar un JOB o TASK que permita calcular periódicamente (todos los días) el pronóstico para ese día y lo almacene en una BBDD (MySQL, PostgreSQL, etc), utilizando Hibernate y Jakarta/JPA para conectarnos, mapear las entidades e interactuar con ella.


## Especificaciones API REST

Se puede consultar:

http://URL:PUERTO/clima/{dia} -> el pronóstico para el día dado
http://URL:PUERTO/clima?anios={anios} -> la cantidad de días en los que se dio cada pronóstico (días normales, sequia, lluvia, dia de máxima lluvia y días con las condiciones optimas de presion y temperatura)

## License

GNU GENERAL PUBLIC LICENSE

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## Host and Cloud Computing

A fin de resolver el requisito opcional, se desplegó la API utilizando los servicios de Google Cloud.

https://challengeabstract.rj.r.appspot.com/

Pruebas:
https://challengeabstract.rj.r.appspot.com/clima?anios=10
Conteo de cada pronóstico para los próximos 10 años

https://challengeabstract.rj.r.appspot.com/clima/128
Pronóstico para el día 128

## Download Project

git clone https://gitlab.com/juanma.91m/challengeabstractmagnaterra.git