package com.challengeabstract.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class SolarSystem {
	private List<Planet> planets;

	public SolarSystem(List<Planet> planets) {
		super();
		this.planets = planets;
	}

	public List<Planet> getPlanets() {
		return new ArrayList<Planet>(this.planets);
	}

	public void setPlanets(List<Planet> planets) {
		this.planets = planets;
	}

	/**
	 * calcula si el sistema solar tuvo sequia para un día determionado
	 * 
	 * @param day: día del cual se requiere calcular la sequia
	 * @return si hubo o no sequía ese día
	 */
	public boolean drought(int day) {
		List<Point> coords = new ArrayList<Point>();
		coords.add(new Point(0.0, 0.0));
		planets.forEach(p -> coords.add(p.calculatePlanetsCoordinate(day)));

		return IntStream.range(1, coords.size() - 1).allMatch(i -> {
			double previousSlope = Math.abs((coords.get(i - 1).getY() - coords.get(i).getY())
					/ (coords.get(i - 1).getX() - coords.get(i).getX()));
			double actualSlope = Math.abs((coords.get(i).getY() - coords.get(i + 1).getY())
					/ (coords.get(i).getX() - coords.get(i + 1).getX()));
			boolean condition = previousSlope == actualSlope;
			return condition;
		});
	}

	/**
	 * calcula si el Sol quedó dentro del tríangulo conformado por los planetas en
	 * un día determionado, determinando si dicho día llovió
	 * 
	 * @param day: día del cual se requiere calcular la lluvia
	 * @return si hubo o no lluvia ese día
	 */
	public boolean sunInTriangle(int day) {
		/*
		 * Para determinar el método matemático para saber si un punto está contenido en
		 * un triángulo (dados las coordenadas de sus 3 vértices) utilice el método de
		 * barycentric coordinates extraido de ChatGPT.
		 */

		List<Point> coords = new ArrayList<Point>();
		planets.forEach(p -> {
			coords.add(p.calculatePlanetsCoordinate(day));
		});

		Double u = ((0.0 - coords.get(2).getX()) * (coords.get(1).getY() - coords.get(2).getY())
				- (coords.get(1).getX() - coords.get(2).getX()) * (0.0 - coords.get(2).getY()))
				/ ((coords.get(0).getX() - coords.get(2).getX()) * (coords.get(1).getY() - coords.get(2).getY())
						- (coords.get(1).getX() - coords.get(2).getX())
								* (coords.get(0).getY() - coords.get(2).getY()));

		Double v = ((coords.get(0).getX() - coords.get(2).getX()) * (0.0 - coords.get(2).getY())
				- (0.0 - coords.get(2).getX()) * (coords.get(0).getY() - coords.get(2).getY()))
				/ ((coords.get(0).getX() - coords.get(2).getX()) * (coords.get(1).getY() - coords.get(2).getY())
						- (coords.get(1).getX() - coords.get(2).getX())
								* (coords.get(0).getY() - coords.get(2).getY()));
		Double w = 1 - u - v;

		return (u >= 0 && v >= 0 && w >= 0 && u <= 1 && v <= 1 && w <= 1);
	}

	/**
	 * calcula el perímetro del tríangulo conformado por los planetas en un día
	 * determionado, dicho método se utiliza para saber cual fue la intensidad de
	 * lluvia, siendo esta proporcional al periodo
	 * 
	 * @param day: día del cual se requiere calcular el perímetro
	 * @return el perímetro para el día dado (intensidad de lluvia)
	 */
	public Double calculatePerimeter(int day) {
		List<Point> coords = new ArrayList<Point>();
		planets.forEach(p -> {
			coords.add(p.calculatePlanetsCoordinate(day));
		});

		Double side1 = coords.get(0).euclideanDistance(coords.get(1));
		Double side2 = coords.get(0).euclideanDistance(coords.get(2));
		Double side3 = coords.get(1).euclideanDistance(coords.get(2));

		return side1 + side2 + side3;
	}

	/**
	 * calcula si los planetas están alineados entre si, sin estar alineados con el
	 * Sol en un día determionado, determinando si dicho día tuvo condiciones
	 * óptimas de temperatura y presión
	 * 
	 * @param day: día del cual se requiere calcular si tuvo condiciones óptimas de
	 *             temperatura y presión
	 * @return si tuvo o no condiciones óptimas de temperatura y presión
	 */
	public boolean optimalPressureAndTemperature(int day) {
		if (this.drought(day))
			return false;

		List<Point> coords = new ArrayList<Point>();
		planets.forEach(p -> {
			coords.add(p.calculatePlanetsCoordinate(day));
		});

		Double x1 = coords.get(0).getX();
		Double x2 = coords.get(1).getX();
		Double x3 = coords.get(2).getX();
		Double y1 = coords.get(0).getY();
		Double y2 = coords.get(1).getY();
		Double y3 = coords.get(2).getY();

		Double areaTriangulo = Math.abs((Double) (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);

		return areaTriangulo.equals(0.0);
	}
}