package com.challengeabstract.model;

public class DayForecast {

	private int dia;
	private Boolean sequia, lluvia, optTempYPresion;

	public DayForecast(int dia) {
		super();
		this.dia = dia;
		this.sequia = false;
		this.lluvia = false;
		this.optTempYPresion = false;
	}

	public Boolean getSequia() {
		return sequia;
	}

	public void setSequia(Boolean sequia) {
		this.sequia = sequia;
	}

	public Boolean getLluvia() {
		return lluvia;
	}

	public void setLluvia(Boolean lluvia) {
		this.lluvia = lluvia;
	}

	public Boolean getOptTempYPresion() {
		return optTempYPresion;
	}

	public void setOptTempYPresion(Boolean OptTempYPresion) {
		this.optTempYPresion = OptTempYPresion;
	}

	/**
	 * Método de ejemplo mediante el cual se podría utilizar para mostrar un
	 * pronostico en algun formato específico también implemetando el toString();
	 * 
	 * @param
	 * @return el clima en formato texto
	 */
	public String getForecast() {
		return sequia ? "Sequía" : lluvia ? "Lluvia" : optTempYPresion ? "Optimo" : "Normal";
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}
}