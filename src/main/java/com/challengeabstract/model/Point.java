package com.challengeabstract.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Point {
	private BigDecimal x, y;

	public Point(Double x, Double y) {
		super();
		this.x = new BigDecimal(x).setScale(1, RoundingMode.HALF_UP);
		this.y = new BigDecimal(y).setScale(1, RoundingMode.HALF_UP);
	}

	public Double getX() {
		return x.doubleValue();
	}

	public void setX(BigDecimal x) {
		this.x = x;
	}

	public Double getY() {
		return y.doubleValue();
	}

	public void setY(BigDecimal y) {
		this.y = y;
	}

	/**
	 * calcula la distancia euclidea (o líneal) que existe entre un punto y otro:
	 * EJ: int distanciaEntrePunto1YPunto2 = punto1.euclideanDistance(punto2);
	 * 
	 * @param punto: punto del cual se desea conocer la distancia
	 * @return distancia euclinea entre el punto que se utilizo para llamar el
	 *         método y el pasado por parámetro
	 */
	public Double euclideanDistance(Point punto) {
		return Math.sqrt(Math.pow(punto.getX() - this.getX(), 2) + Math.pow(punto.getY() - this.getY(), 2));
	}

	@Override
	public String toString() {
		return x + "  ,  " + y;
	}
}