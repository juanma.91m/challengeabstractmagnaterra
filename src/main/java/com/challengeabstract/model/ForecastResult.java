package com.challengeabstract.model;

public class ForecastResult {
	private int normales;
	private int sequias;
	private int cantidadLluvias;
	private int diaMaximaLluvia;
	private int cantidadOptTempYPresion;

	public ForecastResult(int normales, int sequias, int cantidadLluvias, int diaMaximaLluvia,
			int cantidadOptTempYPresion) {
		super();
		this.normales = normales;
		this.sequias = sequias;
		this.cantidadLluvias = cantidadLluvias;
		this.diaMaximaLluvia = diaMaximaLluvia;
		this.cantidadOptTempYPresion = cantidadOptTempYPresion;
	}

	public int getSequias() {
		return sequias;
	}

	public void setSequias(int sequias) {
		this.sequias = sequias;
	}

	public int getNormales() {
		return normales;
	}

	public void setNormales(int normales) {
		this.normales = normales;
	}

	public int getCantidadLluvias() {
		return cantidadLluvias;
	}

	public void setCantidadLluvias(int cantidadLluvias) {
		this.cantidadLluvias = cantidadLluvias;
	}

	public int getDiaMaximaLluvia() {
		return diaMaximaLluvia;
	}

	public void setDiaMaximaLluvia(int diaMaximaLluvia) {
		this.diaMaximaLluvia = diaMaximaLluvia;
	}

	public int getCantidadOptTempYPresion() {
		return cantidadOptTempYPresion;
	}

	public void setCantidadOptTempYPresion(int cantidadOptTempYPresion) {
		this.cantidadOptTempYPresion = cantidadOptTempYPresion;
	}
}