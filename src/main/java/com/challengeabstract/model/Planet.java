package com.challengeabstract.model;

public class Planet {
	private String nombre;
	private Double sunKmDistance;
	private Double angularVelocity;
	private Integer clockwise;

	public Planet(String nombre, Double sunKmDistance, Double angularVelocity, Integer clockwise) {
		super();
		this.nombre = nombre;
		this.sunKmDistance = sunKmDistance;
		this.angularVelocity = angularVelocity;
		this.clockwise = clockwise;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getSunKmDistance() {
		return sunKmDistance;
	}

	public void setSunKmDistance(Double sunKmDistance) {
		this.sunKmDistance = sunKmDistance;
	}

	public Double getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(Double angularVelocity) {
		this.angularVelocity = angularVelocity;
	}

	public Integer getClockwise() {
		return clockwise;
	}

	public void setClockwise(Integer clockwise) {
		this.clockwise = clockwise;
	}

	/**
	 * calcula la posición exacta del planeta en un día determinado
	 * 
	 * @param day: día en el cual se requiere saber la posición
	 * @return Punto que contiene la posición exacta del planeta
	 */
	public Point calculatePlanetsCoordinate(int day) {
		Double angle = day * this.getAngularVelocity() * clockwise % 360;
		double angleInRadians = Math.toRadians(angle);

		Double x = Math.sin(angleInRadians) * this.getSunKmDistance();
		Double y = Math.cos(angleInRadians) * this.getSunKmDistance();

		return new Point(x, y);
	}
}