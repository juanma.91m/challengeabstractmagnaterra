package com.challengeabstract.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.challengeabstract.model.DayForecast;
import com.challengeabstract.model.ForecastResult;
import com.challengeabstract.model.Planet;
import com.challengeabstract.model.SolarSystem;

@RestController
@RequestMapping("/clima")
public class ClimaController {
	private SolarSystem solarSystem;

	public ClimaController() {
		List<Planet> planets = new ArrayList<Planet>();
		planets.add(new Planet("Farengi", 500.0, 1.0, 1));
		planets.add(new Planet("Vulcano", 1000.0, 5.0, -1));
		planets.add(new Planet("Betasoide", 2000.0, 3.0, 1));

		solarSystem = new SolarSystem(planets);
	}

	/**
	 * @param anios: cantidad de años de las cuales se requiere calcular el
	 *               pronostico
	 * @return lista de pronosticos para cada día durante el periodo desde el día 0
	 *         hasta los anios
	 */
	private List<DayForecast> calcularForecasts(int anios) {
		List<DayForecast> forecasts = new ArrayList<DayForecast>();
		for (int i = 0; i < 365 * anios; i++) {

			DayForecast f = new DayForecast(i);
			if (solarSystem.drought(i))
				f.setSequia(true);
			else if (solarSystem.sunInTriangle(i))
				f.setLluvia(true);
			else if (solarSystem.optimalPressureAndTemperature(i))
				f.setOptTempYPresion(true);

			forecasts.add(f);
		}

		return forecasts;
	}

	/**
	 * EJ: http://URL:PUERTO/clima/{dia}
	 * 
	 * @param dia: día del cual se requiere el pronostico
	 * @return el pronóstico para el día dado por parámetro
	 */
	@GetMapping(path = { "/{dia}" })
	private DayForecast calcularForecast(@PathVariable(name = "dia", required = true) int dia) {
		DayForecast f = new DayForecast(dia);
		if (solarSystem.drought(dia))
			f.setSequia(true);
		else if (solarSystem.sunInTriangle(dia))
			f.setLluvia(true);
		else if (solarSystem.optimalPressureAndTemperature(dia))
			f.setOptTempYPresion(true);

		// También se podría retornar sólo un String mostrando la condicion climática
		// haciendo uso del método f.getForecast(); en un formato específico

		return f;
	}

	/**
	 * EJ: http://URL:PUERTO/clima?anios={anios}
	 * 
	 * @param anios: cantidad de años de las cuales se requiere saber cuantos días
	 *               hubo cada condición climática
	 * @return la cantidad de días en los que se dio cada pronóstico (días normales,
	 *         sequia, lluvia, dia de máxima lluvia y días con las condiciones
	 *         optimas de presion y temperatura)
	 */
	@GetMapping
	public ForecastResult getCountForecastsInPeriod(@RequestParam(name = "anios", required = true) int anios) {
		List<DayForecast> forecasts = calcularForecasts(anios);
		int dayMaxRain = 0;
		int droughts = 0;
		int rains = 0;
		int daysOptimalPressureAndTemperature = 0;
		int defaultDays = 0;
		for (DayForecast df : forecasts) {
			if (df.getSequia())
				droughts++;
			else if (df.getLluvia()) {
				rains++;

				Double perimeterMaxRain = solarSystem.calculatePerimeter(dayMaxRain);
				Double actualPerimeter = solarSystem.calculatePerimeter(df.getDia());
				if (actualPerimeter > perimeterMaxRain) {
					dayMaxRain = df.getDia();
					perimeterMaxRain = actualPerimeter;
				}

			} else if (df.getOptTempYPresion())
				daysOptimalPressureAndTemperature++;
			else
				defaultDays++;
		}

		return new ForecastResult(defaultDays, droughts, rains, dayMaxRain, daysOptimalPressureAndTemperature);
	}
}