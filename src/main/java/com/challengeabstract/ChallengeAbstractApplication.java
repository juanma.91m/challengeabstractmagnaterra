package com.challengeabstract;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengeAbstractApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeAbstractApplication.class, args);
	}
}